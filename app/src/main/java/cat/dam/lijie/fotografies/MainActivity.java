package cat.dam.lijie.fotografies;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class MainActivity extends AppCompatActivity{
    private static final String TAG = "ImatgeCapturada";
    static final int REQUEST_PICTURE_CAPTURE = 1;
    private ImageView imatge;
    private String trajImatge;
    private FirebaseStorage firebaseStorage;
    private String idDispositiu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imatge = findViewById(R.id.iv_imatge);
        Button btn_captura = findViewById(R.id.btn_captura);
        btn_captura.setOnClickListener(captura);
        if(!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            btn_captura.setEnabled(false);
        }
        findViewById(R.id.desa_local).setOnClickListener(btn_desaLocal_click);
        findViewById(R.id.desa_nuvol).setOnClickListener(btn_desaNuvol_click);
        FirebaseApp.initializeApp(MainActivity.this);
        firebaseStorage = FirebaseStorage.getInstance();
        getIdDispositiu();
    }
    private View.OnClickListener captura = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
                enviarIntentCapturaImatge();
            }
        }
    };
    private File getPictureFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String pictureFile = "IMG_" + timeStamp;
        //podriem utilitzar el directori public amb getExternalStoragePublicDirectory()
        //enlloc de getExternalFilesDir
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(pictureFile, ".jpg", storageDir);
        trajImatge = image.getAbsolutePath();
        return image;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PICTURE_CAPTURE && resultCode == RESULT_OK) {
            File imgFile = new File(trajImatge);
            if(imgFile.exists()) {
                imatge.setImageURI(Uri.fromFile(imgFile));
            }
        }
    }
    //desa la imatge capturada a la memòria local
    private View.OnClickListener btn_desaLocal_click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            desarALocal();
        }
    };
    //desa la imatge capturada al núvol
    private View.OnClickListener btn_desaNuvol_click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            desarAlNuvol();
        }
    };
    protected synchronized String getIdDispositiu() {
        if (idDispositiu == null) {
            SharedPreferences sharedPrefs = this.getSharedPreferences(
                    "DEVICE_ID", Context.MODE_PRIVATE);
            idDispositiu = sharedPrefs.getString("DEVICE_ID", null);
            if (idDispositiu == null) {
                idDispositiu = UUID.randomUUID().toString();
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString("DEVICE_ID", idDispositiu);
                editor.commit();
            }
        }
        return idDispositiu;
    }
//incloure codi enviarIntentCapturaImatge, desarALocal, desarAlNuvol

    private void enviarIntentCapturaImatge() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra( MediaStore.EXTRA_FINISH_ON_COMPLETION, true);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE);
            File pictureFile = null;
            try {
                pictureFile = getPictureFile();
            } catch (IOException ex) {
                Toast.makeText(this,
                        "ERROR: No s'ha pogut crear el fitxer d'imatge. Torneu-ho a provar.",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            if (pictureFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "cat.dam.lijie.fotografies",
                        pictureFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE);
            }
        }
    }
    private void desarALocal() {
        Intent intentDesarlocal = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(trajImatge);
        Uri picUri = Uri.fromFile(f);
        intentDesarlocal.setData(picUri);
        this.sendBroadcast(intentDesarlocal);
    }
    //desa la imatge capturada al núvol
    private void desarAlNuvol() {
        File f = new File(trajImatge);
        Uri uriImatge = Uri.fromFile(f);
        final String cloudFilePath = idDispositiu + uriImatge.getLastPathSegment();
        //cal desactivar accés només amb autorització en els permisos de Firebase/Storage
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        StorageReference storageRef = firebaseStorage.getReference();
        StorageReference uploadeRef = storageRef.child(cloudFilePath);
        uploadeRef.putFile(uriImatge).addOnFailureListener(new OnFailureListener(){
            public void onFailure(@NonNull Exception exception){
                Log.e(TAG,"ERROR: No s'ha pogut desar la imatge al núvol");
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>(){
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot){
                Toast.makeText(MainActivity.this,
                        "La imatge ha estat desada al núvol",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}